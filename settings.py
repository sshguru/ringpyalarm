# THIS IS A NEW LINE
# this is a branch

# hour of the day (24h format) when the system should engage
monitoring_start_hour = 23
# hour of the day (24h format) when the system should disangage
monitoring_end_hour = 6
# maximum time between motion events for the system to trigger an alarm
# important: 150 is the minimum realistic value as Ring events are always at least 2min apart
monitoring_threshold = 150
# name of the device which the program should use for detecting motion events (as showing up in Ring app)
monitoring_device = "Garden Cam"
# name of the chime which the program should use for ringing the alarm (as showing up in Ring app)
alarm_device = "Downstairs Chime"
# name of the tune to play when motion is confitmed (detected 3 times in a row)
alarm_tune = ['ding']
# how many times the tune above should ring
alarm_repeat = 3
# setting this to true will dump "no alarm"/"alarm" messages to standard output
display_output = False
