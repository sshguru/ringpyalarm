# RingPyAlarm
Monitors motion events on a specified Ring camera/doorbell and plays an alarm on a chosen Chime. The alarm will ring when 3 motion event are detected with user-configurable time between them (and only during the time of day specified in settings; i.e. 11pm and 6am)

# Installation & Configuration
1. Clone the repo
2. Install `ring_doorbell` module:
```
python3 -m pip install ring_doorbell
```
3. Edit the settings.py according to your needs
4. Run with:
```
python3 ringpyalarm.py
```
5. Input username and password, followed by 2FA PIN when prompted