import json
import time
import getpass
from pathlib import Path
from datetime import datetime
from ring_doorbell import Ring, Auth
from oauthlib.oauth2 import MissingTokenError
from settings import *


def main():
    if Path("token.cache").is_file():
        auth = Auth("RingPyAlarm/1.0", json.loads(Path("token.cache").read_text()), token_updated)
    else:
        username = input("Username: ")
        password = getpass.getpass("Password: ")
        auth = Auth("RingPyAlarm/1.0", None, token_updated)
        try:
            auth.fetch_token(username, password)
        except MissingTokenError:
            auth.fetch_token(username, password, otp_callback())
    ring = Ring(auth)
    ring.update_data()
    chime, camera = find_devices(ring, alarm_device, monitoring_device)
    while True:
        delta1, delta2 = check_alerts(camera)
        if is_hour_between(monitoring_start_hour, monitoring_end_hour) and delta1 < monitoring_threshold and delta2 < monitoring_threshold:
            if display_output is True:
                print('ALARM!!!')
            start_alarm(chime)
            time.sleep(300)
        else:
            if display_output is True:
                print('no alarm')

def check_alerts(device):
    alerts = []
    for event in device.history(limit=3):
        alert = event['created_at']
        alerts.append(alert)
    delta1 = str((alerts[0] - alerts[1]).total_seconds())
    delta2 = str((alerts[1] - alerts[2]).total_seconds())
    delta1_int = int(delta1[:len(delta1)-2])
    delta2_int = int(delta2[:len(delta2)-2])
    return delta1_int, delta2_int

def start_alarm(chime):
    try:
        for i in range(1,alarm_repeat):
            chime.test_sound(kind = alarm_tune)
            time.sleep(5)
    except:
        print("chime offline")

def is_hour_between(start, end):
    now = datetime.now()
    now = int(now.strftime("%H"))
    is_between = False
    is_between |= start <= now <= end
    is_between |= end < start and (start <= now or now <= end)
    return is_between

def load_devices(ring):
    try:
        devices = ring.devices()
    except:
        print("ring devices unavaialable")
    try:
        doorbells = devices['doorbots']
    except:
        print("all doorbells offline")
    try:
        cameras = devices['stickup_cams']
    except:
        print("all cameras offline")
    try:
        chimes = devices['chimes']
    except:
        print("all chimes offline")
    return chimes, doorbells, cameras

def find_devices(ring, target_chime, target_camera):
    chimes, doorbells, cameras = load_devices(ring)
    for chime in chimes:
        chime_name = get_device_name(chime)
        if chime_name == target_chime:
            found_chime = chime
    for doorbell in doorbells:
        doorbell_name = get_device_name(doorbell)
        if doorbell_name == target_camera:
            found_camera = doorbell
    for camera in cameras:
        camera_name = get_device_name(camera)
        if camera_name == target_camera:
            found_camera = camera
    return found_chime, found_camera

def get_device_name(device):
    name = ""
    words = str(device).split(" ")
    for i in range(1,len(words)):
        name = f"{name}{words[i]} "
    return name.replace(">","").strip()

def token_updated(token):
    Path("token.cache").write_text(json.dumps(token))

def otp_callback():
    auth_code = input("2FA code: ")
    return auth_code

if __name__ == "__main__":
    main()